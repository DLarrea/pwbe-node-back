module.exports = app => {
    const categoriaProducto = require("../controllers/categoriaproductodao.controller.js");
    var router = require("express").Router();
    router.post("/", categoriaProducto.create);
    router.get("/", categoriaProducto.findAll);
    router.get("/:id", categoriaProducto.findOne);
    router.put('/:id', categoriaProducto.update);
    router.delete('/:id', categoriaProducto.delete);
    app.use('/api/categoria-producto', router);
};
