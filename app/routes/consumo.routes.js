module.exports = app => {
    const consumo = require("../controllers/consumodao.controller.js");
    var router = require("express").Router();
    router.post("/", consumo.create);
    router.put('/:id', consumo.update);
    router.post("/detalle", consumo.createDetalle);
    router.delete('/detalle/:id', consumo.deleteDetalle);
    router.get("/", consumo.findAll);
    router.get("/mesas", consumo.findAllMesas);
    app.use('/api/consumo', router);
};
