const db = require("../models");
const Producto = db.Producto;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
    // Validate request
    if (!req.body.categoria) {
        res.status(400).send({
            message: "Categoria requerida"
        });
        return;
    }
    if (!req.body.precio) {
        res.status(400).send({
            message: "Precio requerido"
        });
        return;
    }
    if (!req.body.nombre) {
        res.status(400).send({
            message: "Nombre requerido"
        });
        return;
    }
    // crea una mesa
    const producto = {
        nombre: req.body.nombre,
        precio: req.body.precio,
        CategoriaProductoId: req.body.categoria
    };
    // Guardamos a la base de datos
    Producto.create(producto)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al guardar el producto."
            });
        });
};

exports.findOne = (req, res) => {
    const id = req.params.id;
    Producto.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al obtener el producto con id=" + id
            });
        });
};

exports.findAll = (req, res) => {
    
    Producto.findAll()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ocurrio un error al obtener productos."
            });
        });
};

exports.update = (req, res) => {
    const id = req.params.id;
  
    Producto.update(req.body, { where: { id: id } })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "El producto se ha actualizado correctamente."
                });
            } else {
                res.send({
                    message: `Ocurrio un error. No se pudo actualizar el producto con id= ${id}.!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error actualizando el producto con id= " + id
            });
        });
};

exports.delete = (req, res) => {
    
    const id = req.params.id;
  
    Producto.destroy({ where: { id: id } })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "El producto fue borrado correctamente!"
                });
            } else {
                res.send({
                    message: `Ocurrio un error. No se pudo borar el producto con id= ${id}.!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error borrando el producto con id= " + id
            });
        });
};
