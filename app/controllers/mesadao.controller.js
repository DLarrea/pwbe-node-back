const db = require("../models");
const Mesa = db.Mesa;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
    // Validate request
    if (!req.body.posicionx) {
        res.status(400).send({
            message: "Debe ingresar la posicion de la Mesa!"
        });
        return;
    }
    if (!req.body.posiciony) {
        res.status(400).send({
            message: "Debe ingresar la posicion de la Mesa!"
        });
        return;
    }
    if (!req.body.capacidad) {
        res.status(400).send({
            message: "Debe ingresar la capacidad de la Mesa!"
        });
        return;
    }
    // crea una mesa
    const mesa = {
        nombre: req.body.nombre,
        posicionx: req.body.posicionx,
        posiciony: req.body.posiciony,
        planta: req.body.planta,
        capacidad: req.body.capacidad,
        RestauranteId: req.body.RestauranteId
    };
    // Guardamos a la base de datos
    Mesa.create(mesa)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al guardar la Mesa."
            });
        });
};

exports.findOne = (req, res) => {
    const id = req.params.id;
    Mesa.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al obtener mesa con id=" + id
            });
        });
};

exports.findAll = (req, res) => {
    const RestauranteId = req.query.restaurante;
    //var condition = RestauranteId ? { RestauranteId: { [Op.eq]: `${RestauranteId}` } } : null;
    console.log(req.query);
    var mesa = req.query.mesas;
    if(mesa){
        if (!Array.isArray(mesa)) {
            mesa=[mesa];
        }
        mesa = mesa.map(Number);
    }else{
        mesa = [];
    }
        
    /* const mesa = JSON.parse(mes.split(" ").join(""));
    console.log(mesas);
    console.log(mesas.length); */
    Mesa.findAll({ include: ["Restaurante"], where: {id: {[Op.notIn]: mesa}, RestauranteId: RestauranteId},
        order: [
            ['planta', 'ASC'],
        ],
    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ocurrio un error al obtener los Mesas."
            });
        });
};

exports.update = (req, res) => {
    const id = req.params.id;
  
    Mesa.update(req.body, { where: { id: id } })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "La Mesa se ha actualizado correctamente."
                });
            } else {
                res.send({
                    message: `Ocurrio un error. No se pudo actualizar Mesa con id= ${id}.!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error actualizando la Mesa con id= " + id
            });
        });
};

exports.delete = (req, res) => {
    const id = req.params.id;
  
    Mesa.destroy({ where: { id: id } })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "La Mesa fue borrado correctamente!"
                });
            } else {
                res.send({
                    message: `Ocurrio un error. No se pudo borar la Mesa con id= ${id}.!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error borrando la Mesa con id= " + id
            });
        });
};
