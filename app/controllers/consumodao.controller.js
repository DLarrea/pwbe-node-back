const db = require("../models");
const ConsumoCabecera = db.ConsumoCabecera;
const ConsumoDetalle = db.ConsumoDetalle;
const Mesa = db.Mesa;
const Restaurante = db.Restaurante;
const Cliente = db.Cliente;
const Producto = db.Producto;
const Op = db.Sequelize.Op;

// Create cabecera consumo
exports.create = (req, res) => {
    // Validate request
    let cabecera = req.body.cabecera;
    let detalles = req.body.detalles;
    let precioTotal = 0;
    // Validacion cabecera
    if (!cabecera.cliente) {
        res.status(400).send({
            message: "Cliente requerida"
        });
        return;
    }
    if (!cabecera.mesa) {
        res.status(400).send({
            message: "Mesa requerida"
        });
        return;
    }
    if (!cabecera.creacionConsumo) {
        res.status(400).send({
            message: "Creacion consumo requerido"
        });
        return;
    }

    for(let i=0; i<detalles.length;i++){
        let det = detalles[i];
        if(!det.producto || !det.cantidad){
            res.status(400).send({
                message: "Detalles incompletos"
            });
            return;
        }
        precioTotal += det.productoPrecio * det.cantidad;
    }
    // crea una mesa
    const consumoCabecera = {
        ClienteId: cabecera.cliente,
        MesaId: cabecera.mesa,
        estado: "A",
        creacionConsumo: cabecera.creacionConsumo,
        cierreConsumo: null,
        total: precioTotal
    };
    // Guardamos a la base de datos
    ConsumoCabecera.create(consumoCabecera)
        .then(data => {
            let idCabecera = data.id;
            detalles.forEach(el => {
                let det = {
                    cantidad: el.cantidad,
                    ProductoId: el.producto,
                    ConsumoCabeceraId: idCabecera
                }
                ConsumoDetalle.create(det);
            });
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al guardar el consumo"
            });
        });
};

// Update Cliente Consumo Cabecera
exports.update = (req, res) => {
    const id = req.params.id;
  
    ConsumoCabecera.update(req.body, { where: { id: id } })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "El consumo se ha actualizado correctamente."
                });
            } else {
                res.send({
                    message: `Ocurrio un error. No se pudo actualizar el consumo con id= ${id}.!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error actualizando el consumo con id= " + id
            });
        });
};

// Create consumo detalle
exports.createDetalle = (req, res) => {
    // Validate request
    if (!req.body.producto) {
        res.status(400).send({
            message: "Producto requerido"
        });
        return;
    }
    if (!req.body.cantidad) {
        res.status(400).send({
            message: "Cantidad requerida"
        });
        return;
    }
    if (!req.body.cabecera) {
        res.status(400).send({
            message: "Cabecera requerida"
        });
        return;
    }
    
    // crea una mesa
    const consumoDetalle = {
        ConsumoCabeceraId: req.body.cabecera,
        ProductoId: req.body.producto,
        cantidad: req.body.cantidad
    };
    // Guardamos a la base de datos
    ConsumoDetalle.create(consumoDetalle)
        .then(data => {
            let totalBody = {
                total: req.body.total + consumoDetalle.cantidad * req.body.productoPrecio
            }
            ConsumoCabecera.update(totalBody, { where: { id: req.body.cabecera } });
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al guardar el consumo detalle"
            });
        });
};

// Delete consumo detalle
exports.deleteDetalle = (req, res) => {
    
    const id = req.params.id;
    const total = Number(req.query.total);
    const cabecera = Number(req.query.cabecera);
  
    ConsumoDetalle.destroy({ where: { id: id } })
        .then(num => {
            if (num == 1) {
                let totalBody = {
                    total: total
                }
                console.log("##################################",totalBody);
                ConsumoCabecera.update(totalBody, { where: { id: cabecera } });
                res.send({
                    message: "El detalle fue borrado correctamente!"
                });
            } else {
                res.send({
                    message: `Ocurrio un error. No se pudo borar el detalle con id= ${id}.!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error borrando el detalle con id= " + id
            });
        });
};

// Find by mesa
exports.findAll = (req, res) => {

    var query = {
        where: {id: req.query.mesa},
        include: [
            {
                model: db.ConsumoCabecera, as: 'consumo',
                include: [
                    {
                        model: db.ConsumoDetalle,
                        as: 'detalles',
                        include: [
                            {
                                model: Producto,
                                as: 'Producto'
                            }
                        ]
                    },
                    {
                        model: Cliente,
                        as: "Cliente"
                    }
                ]
            }
        ]
    }

    Mesa.findOne(query)
    .then( data => {
        let consumo = data.consumo;
        res.send(consumo.filter(el => {
            return el.estado === "A"
        }));
        // res.send(consumo.filter(el => {
        //     return el.estado === "A"
        // }));
    })
    .catch(err => {
        res.status(500).send({
            message:
                err.message || "Ocurrio un error al obtener consumo."
        });
    });
};

// Get all Mesas
exports.findAllMesas = (req, res) => {
    var query = {
        include: [{
            model: Restaurante,
            as: "Restaurante"
        }]
    }
    Mesa.findAll(query)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ocurrio un error al obtener mesas."
            });
        });
};


