const db = require("../models");
const CategoriaProducto = db.CategoriaProducto;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
    // Validate request
    if (!req.body.nombre) {
        res.status(400).send({
            message: "Nombre requerido"
        });
        return;
    }
    // crea un CategoriaProducto
    const categoriaProducto = {
        nombre: req.body.nombre,
    };
    // Guardamos a la base de datos
    CategoriaProducto.create(categoriaProducto)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al guardar la categoria de producto."
            });
        });
};

exports.findOne = (req, res) => {
    const id = req.params.id;
    CategoriaProducto.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al obtener venta con id=" + id
            });
        });
};

exports.findAll = (req, res) => {
    CategoriaProducto.findAll()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ocurrio un error al obtener categorias."
            });
        });
};

exports.update = (req, res) => {
    const id = req.params.id;
  
    CategoriaProducto.update(req.body, { where: { id: id } })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Categoria actualizada correctamente."
                });
            } else {
                res.send({
                    message: `Ocurrio un error. No se pudo actualizar categoria con id= ${id}.!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error actualizando la categoria con id= " + id
            });
        });
};

exports.delete = (req, res) => {
    const id = req.params.id;
  
    CategoriaProducto.destroy({ where: { id: id } })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Categoria eliminada"
                });
            } else {
                res.send({
                    message: `Ocurrio un error. No se pudo borar la categoria con id= ${id}.!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error borrando la categoria con id= " + id
            });
        });
};