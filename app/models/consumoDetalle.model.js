module.exports = (sequelize, Sequelize) => {
    const ConsumoDetalle = sequelize.define("ConsumoDetalle", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        cantidad: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    });
    return ConsumoDetalle;
};
