module.exports = (sequelize, Sequelize) => {
    const ConsumoCabecera = sequelize.define("ConsumoCabecera", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        total: {
            type: Sequelize.INTEGER,
            allowNull: false
        },   
        creacionConsumo: {
            type: Sequelize.STRING,
            allowNull: false
        },   
        cierreConsumo: {
            type: Sequelize.STRING,
            defaultValue: null
        },
        estado: {
            type: Sequelize.STRING(1),
            defaultValue: "A"
        }
    });
    return ConsumoCabecera;
};
