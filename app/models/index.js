const dbConfig = require("../config/db.config.js");
const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,
  port: dbConfig.PORT,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.Restaurante = require("./restaurante.model.js")(sequelize, Sequelize);
db.Mesa = require("./mesa.model.js")(sequelize, Sequelize);
db.Reserva = require("./reserva.model.js")(sequelize, Sequelize);
db.Cliente = require("./cliente.model.js")(sequelize, Sequelize);
db.CategoriaProducto = require("./categoriaProducto.model.js")(sequelize, Sequelize);
db.Producto = require("./producto.model.js")(sequelize, Sequelize);
db.ConsumoCabecera = require("./consumoCabecera.model.js")(sequelize, Sequelize);
db.ConsumoDetalle = require("./consumoDetalle.model.js")(sequelize, Sequelize);


db.Restaurante.hasMany(db.Mesa, { as: "mesas" });
db.Mesa.belongsTo(db.Restaurante, {
  foreignKey: "RestauranteId",
  as: "Restaurante",
}); 

db.Restaurante.hasMany(db.Reserva, { as: "reservas" });
db.Reserva.belongsTo(db.Restaurante, {
  foreignKey: "RestauranteId",
  as: "Restaurante",
});
db.Mesa.hasMany(db.Reserva, { as: "reservas" });
db.Reserva.belongsTo(db.Mesa, {
  foreignKey: "MesaId",
  as: "Mesa",
});
db.Cliente.hasMany(db.Reserva, { as: "reservas" });
db.Reserva.belongsTo(db.Cliente, {
  foreignKey: "ClienteId",
  as: "Cliente",
}); 

db.CategoriaProducto.hasMany(db.Producto, { as: "productos" });
db.Producto.belongsTo(db.CategoriaProducto, {
  foreignKey: "CategoriaProductoId",
  as: "CategoriaProducto",
}); 


db.Cliente.hasMany(db.ConsumoCabecera, { as: "consumo" });
db.ConsumoCabecera.belongsTo(db.Cliente, {
  foreignKey: "ClienteId",
  as: "Cliente",
}); 

db.Mesa.hasMany(db.ConsumoCabecera, { as: "consumo" });
db.ConsumoCabecera.belongsTo(db.Mesa, {
  foreignKey: "MesaId",
  as: "Mesa",
}); 


db.ConsumoCabecera.hasMany(db.ConsumoDetalle, { as: "detalles" });
db.ConsumoDetalle.belongsTo(db.ConsumoCabecera, {
  foreignKey: "ConsumoCabeceraId",
  as: "ConsumoCabecera",
}); 

db.Producto.hasMany(db.ConsumoDetalle, { as: "detalles" });
db.ConsumoDetalle.belongsTo(db.Producto, {
  foreignKey: "ProductoId",
  as: "Producto",
}); 

module.exports = db;
