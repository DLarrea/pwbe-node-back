module.exports = (sequelize, Sequelize) => {
    const CategoriaProducto = sequelize.define("CategoriaProducto", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nombre: {
            type: Sequelize.STRING
        }  
    });
    return CategoriaProducto;
};
