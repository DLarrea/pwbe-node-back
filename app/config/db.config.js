module.exports = {
  HOST: "localhost",
  USER: "postgres",
  PASSWORD: "postgres",
  DB: "tp2",
  PORT: "5433",
  dialect: "postgres",
  pool: {
    max: 30,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
